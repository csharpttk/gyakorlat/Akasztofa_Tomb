﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Akasztófa_Tömb
{
    class Program
    {
        static string inic( string kitalálandó)
        {
            return new String('-', kitalálandó.Length); 
            //szövegtípus hosszaszor ismétlődik a '-' jel
        }
        static void következőtipp(string kitalálandó, ref string közbülső, ref int hibapont)
        {
            string segéd = ""; //itt építjük fel az új közbülső eredményt
            Console.Write("Melyik betűvel próbálkozik? ");
            char tipp = Convert.ToChar(Console.ReadLine());
            for (int i = 0; i < kitalálandó.Length; i++)
            {
                if (tipp == kitalálandó[i]) { segéd += tipp; }//konkatenáció
                else { segéd += közbülső[i]; }
            }
            if (közbülső == segéd) hibapont++;
            közbülső = segéd;
        }
        static void Main(string[] args)
        {
            string[] szavak = { "almafa", "programozás", "verzió", "tesztelés", "függvények", "ciklusok" };
            Random r = new Random(); int melyik = r.Next(szavak.Length);
            string kitalálandó = szavak[melyik], közbülső;
            char tipp;
            int hibapont = 0;
            közbülső = inic(kitalálandó); //annyi '-', ahány hosszú a kitalálandó
            while ( kitalálandó != közbülső && hibapont < 9 )
            {
                következőtipp(kitalálandó, ref közbülső, ref hibapont);
                Console.WriteLine("{0} eddigi hibák száma: {1}", közbülső,hibapont.ToString());               
            }
            if (hibapont == 9) { Console.WriteLine("Vesztett"); }
            else { Console.WriteLine("Gratulálok nyert"); }
        }
    }
}
